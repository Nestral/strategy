﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Building : MonoBehaviour
{

    public Tile Tile { get; private set; }
    public abstract bool Movable { get; }

    public Map Map => Tile.Map;

    public abstract Dictionary<Resource, int> BuildCostDictionary { get; }
    public virtual Transform UpgradeTo { get; } = null;

    public void MoveBuilding(Vector2 newPosition)
    {
        MoveBuilding(Map.Tiles[newPosition]);
    }

    public void MoveBuilding(Tile newTile)
    {
        Tile = newTile;
        newTile.Building = this;

        transform.parent = newTile.transform;
        transform.position = newTile.transform.position;
    }

    public void RemoveBuilding()
    {
        Tile.Building = null;
        Tile = null;
        Destroy(gameObject);
    }

}
