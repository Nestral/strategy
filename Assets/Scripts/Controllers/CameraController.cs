﻿using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : NetworkBehaviour
{
    public Camera Camera;

    public float ScrollingSpeed = 10f;
    public float MinCameraSize = 2f;
    public float MaxCameraSize = 10f;

    private Vector2 originMousePos;

    private void Start()
    {
        if (!isLocalPlayer)
        {
            Camera.enabled = false;
            Camera.GetComponent<AudioListener>().enabled = false;
            return;
        }

        Camera.enabled = true;
        Camera.GetComponent<AudioListener>().enabled = true;

        originMousePos = Input.mousePosition;
    }

    private void Update()
    {
        if (!isLocalPlayer)
            return;

        Vector2 mousePos = Input.mousePosition;

        if (Input.GetMouseButton(0))
        {
            transform.Translate(Camera.ScreenToWorldPoint(originMousePos) - Camera.ScreenToWorldPoint(mousePos));
        }

        float scroll = -Input.mouseScrollDelta.y * Time.deltaTime;
        Camera.orthographicSize = Mathf.Clamp(Camera.orthographicSize + scroll * ScrollingSpeed, MinCameraSize, MaxCameraSize);

        originMousePos = mousePos;
    }

}
