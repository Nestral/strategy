﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Road : Building
{

    public override bool Movable => false;

    public override Dictionary<Resource, int> BuildCostDictionary => BuildCost;

    private static readonly Dictionary<Resource, int> BuildCost = new Dictionary<Resource, int>()
    {
        { Resource.Wood, 1 },
        { Resource.Clay, 1 }
    };

}
