﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileForest : Tile
{

    public override Resource Resource => Resource.Wood;

}
