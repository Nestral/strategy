﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Village : Building
{
    [HideInInspector] public Transform TownPrefab => Map.Generator.TownPrefab;

    public override bool Movable => false;

    public override Dictionary<Resource, int> BuildCostDictionary => BuildCost;

    private static readonly Dictionary<Resource, int> BuildCost = new Dictionary<Resource, int>()
    {
        { Resource.Wood, 2 },
        { Resource.Wheat, 1 },
        { Resource.Clay, 1 }
    };

    public override Transform UpgradeTo => TownPrefab;

}
