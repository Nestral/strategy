﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileMountain : Tile
{

    public override Resource Resource => Resource.Stone;

    public bool RiverStarts = false;
    public float RiverStartChance = 0.05f;

    private void Awake()
    {
        RiverStarts = UnityEngine.Random.value <= RiverStartChance;
    }

}
