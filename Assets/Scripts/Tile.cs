﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public abstract class Tile : MonoBehaviour
{
    public Vector2 Position { get; set; }
    internal Map Map { get; set; }
    public abstract Resource Resource { get; }

    public Building Building;

    public Tile[] GetNeighbourTiles()
    {
        List<Tile> tiles = new List<Tile>();
        foreach (Vector2 coord in NeighbourCoords)
        {
            if (Map.Tiles.TryGetValue(Position + coord, out Tile tile))
                tiles.Add(tile);
        }
        return tiles.ToArray();
    }

    public static Vector2[] NeighbourCoords =
    {
        new Vector2(1, 0),
        new Vector2(1, -1),
        new Vector2(0, -1),
        new Vector2(-1, 0),
        new Vector2(-1, 1),
        new Vector2(0, 1)
    };

    public static Vector2 GetRandomDirection()
    {
        return NeighbourCoords[UnityEngine.Random.Range(0, NeighbourCoords.Length)];
    }

    public Tile[] GetForwardTiles(Vector2 directionFrom)
    {
        List<Tile> tiles = new List<Tile>();
        Vector2[] forwardDirs = GetForwardDirections(directionFrom);
        foreach (Vector2 direction in forwardDirs)
        {
            if (Map.Tiles.TryGetValue(Position + direction, out Tile tile))
                tiles.Add(tile);
        }
        return tiles.ToArray();
    }

    public static Vector2[] GetForwardDirections(Vector2 directionFrom)
    {
        IEnumerable<Vector2> shiftedCoords = NeighbourCoords.Select(coord => coord + directionFrom);
        return NeighbourCoords.Where(coord => coord != directionFrom && !shiftedCoords.Contains(coord)).ToArray();
    }

    private Vector3 mousePosMemory;

    private void OnMouseDown()
    {
        mousePosMemory = Input.mousePosition;
    }

    private void OnMouseUp()
    {
        if (mousePosMemory != Input.mousePosition)
            return;

        Map.Editor.SelectTile(this);
    }

}
