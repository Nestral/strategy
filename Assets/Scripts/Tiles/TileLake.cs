﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileLake : Tile
{

    public override Resource Resource => Resource.Clay;

    public bool RiverStarts = false;
    public float RiverStartChance = 0.05f;

    private void Awake()
    {
        RiverStarts = UnityEngine.Random.value <= RiverStartChance;
    }

}
