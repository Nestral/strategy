﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game : MonoBehaviour
{
    public Player[] Players;

    public float StepTimeLimit;
    public int CurrentPlayerIndex;
    public Player CurrentPlayer;

    [HideInInspector] public float StepTimer;

    public void StartGame()
    {
        SetPlayerTurn(0);
    }

    public void NextTurn()
    {
        CurrentPlayerIndex++;
        if (CurrentPlayerIndex >= Players.Length)
        {
            CurrentPlayerIndex = 0;
        }
        CurrentPlayer = Players[CurrentPlayerIndex];
    }

    private void SetPlayerTurn(int playerIndex)
    {
        CurrentPlayerIndex = Mathf.Clamp(playerIndex, 0, Players.Length);
        CurrentPlayer = Players[CurrentPlayerIndex];
    }

}
