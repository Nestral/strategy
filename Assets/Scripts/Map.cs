﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Map : MonoBehaviour
{
    public Dictionary<Vector2, Tile> Tiles { get; } = new Dictionary<Vector2, Tile>();

    [HideInInspector] public MapGenerator Generator;
    [HideInInspector] public MapCustomEditor Editor;

    private void Awake()
    {
        Generator = GetComponent<MapGenerator>();
        Editor = GetComponent<MapCustomEditor>();
    }

}
