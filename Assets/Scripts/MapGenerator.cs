﻿using Mirror;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class MapGenerator : NetworkBehaviour
{
    public Transform TileRiverPrefab;
    public Transform TileForestPrefab;
    public Transform TileDesertPrefab;
    public Transform TilePlainsPrefab;
    public Transform TileOceanPrefab;
    public Transform TileMountainPrefab;
    public Transform TileLakePrefab;

    public Transform VillagePrefab;
    public Transform TownPrefab;
    public Transform RoadPrefab;
    public Transform ShipPrefab;

    public Map Map;
    public int MapSize;
    public int OceanSize;

    public float TileSpace;
    public float MapBorderRandomFactor;

    private Dictionary<Vector2, int> tilesDistance = new Dictionary<Vector2, int>();
    private Dictionary<Transform, float> tilesFrequencies;

    private void Awake()
    {
        tilesFrequencies = new Dictionary<Transform, float>()
        {
            {TileForestPrefab, 5/12f},
            {TilePlainsPrefab, 5/12f},
            //{TileRiverPrefab, 7/30f},
            {TileLakePrefab, 1/20f},
            {TileDesertPrefab, 1/20f},
            {TileMountainPrefab, 1/15f}
        };

        Map = GetComponent<Map>();
    }

    private void Start()
    {
        GenerateMap();
    }

    public void ResetMap()
    {
        foreach (Tile tile in Map.Tiles.Values)
        {
            Destroy(tile.gameObject);
        }

        Map.Tiles.Clear();
        tilesDistance.Clear();
    }

    public void GenerateMap()
    {
        ResetMap();

        Tile startTile = SpawnNewTile(TilePlainsPrefab, Vector2.zero);
        tilesDistance.Add(startTile.Position, 0);

        GenerateBorder(0);
        GenerateTiles(TileMountainPrefab, tilesFrequencies[TileMountainPrefab]);
        GenerateTiles(TileLakePrefab, tilesFrequencies[TileLakePrefab]);
        GenerateRivers();
        GenerateTiles(TileDesertPrefab, tilesFrequencies[TileDesertPrefab]);
        GenerateTiles(TileForestPrefab, tilesFrequencies[TileForestPrefab]);
        FillMap(TilePlainsPrefab);
        FillOcean((int)(MapSize - MapBorderRandomFactor));

        //GenerateTiles(1);
        //GenerateOcean(MapSize);
    }

    private void GenerateTiles(Transform prefab, float chance)
    {
        float limit = tilesDistance.Count * chance;
        for (int i = 0; i < limit; i++)
        {
            Vector2 pos = tilesDistance.Keys.ElementAt(UnityEngine.Random.Range(0, tilesDistance.Count));
            if (!Map.Tiles.ContainsKey(pos))
                SpawnNewTile(prefab, pos);
            else
                i--;
        }
    }

    private void FillMap(Transform tilePlainsPrefab)
    {
        foreach (Vector2 position in tilesDistance.Keys)
        {
            if (!Map.Tiles.ContainsKey(position))
                SpawnNewTile(tilePlainsPrefab, position);
        }
    }

    private void GenerateRivers()
    {
        Tile[] riverTiles = Map.Tiles.Where
            (tile => tile.Value is TileLake
            || (tile.Value is TileMountain mountain && mountain.RiverStarts))
            .Select(tile => tile.Value).ToArray();

        foreach (Tile tile in riverTiles)
        {
            GenerateRiver(tile);
        }
    }

    private void GenerateRiver(Tile startTile)
    {
        Vector2 currentPos = startTile.Position;

        //Set start river direction
        Vector2 dir = Tile.GetRandomDirection();
        Vector2 riverPos = currentPos + dir;
        while (Map.Tiles.ContainsKey(riverPos))
        {
            if (Map.Tiles[riverPos] is TileRiver || Map.Tiles[riverPos] is TileLake)
                return;

            dir = Tile.GetRandomDirection();
            riverPos = currentPos + dir;
        }
        currentPos = riverPos;

        //Check if we reached outside of the map
        if (!tilesDistance.ContainsKey(riverPos))
            return;

        TileRiver riverStart = SpawnNewTile(TileRiverPrefab, riverPos) as TileRiver;

        //Check if we got stuck between tiles
        if (Map.Tiles[currentPos].GetForwardTiles(dir).Length == Tile.GetForwardDirections(dir).Length)
            return;

        //Generate the rest of the river
        for (int i = 0; i < 50; i++)
        {
            Vector2 nextDir;

            nextDir = riverStart.SetRandomEndDirection(dir);
            riverPos = currentPos + nextDir;

            while (Map.Tiles.ContainsKey(riverPos))
            {
                Tile checkTile = Map.Tiles[riverPos];
                if (checkTile is TileRiver || checkTile is TileLake)
                    return;

                nextDir = riverStart.SetRandomEndDirection(dir);
                riverPos = currentPos + nextDir;
            }
            dir = -nextDir;
            currentPos = riverPos;

            //Check if we reached outside of the map
            if (!tilesDistance.ContainsKey(riverPos))
                return;

            riverStart = SpawnNewTile(TileRiverPrefab, riverPos) as TileRiver;
            riverStart.DirectionStart = dir;

            //Check if we got stuck between tiles
            if (Map.Tiles[currentPos].GetForwardTiles(dir).Length == Tile.GetForwardDirections(dir).Length)
                return;
        }
    }

    private void GenerateBorder(int distance)
    {
        Vector2[] tilesPos = tilesDistance.Where(kvp => kvp.Value == distance - 1).Select(kvp => kvp.Key).ToArray();

        foreach (Vector2 position in tilesPos)
        {
            foreach (Vector2 coord in Tile.NeighbourCoords)
            {
                if (distance > MapSize + UnityEngine.Random.Range(-1f, 1f) * MapBorderRandomFactor)
                    break;

                Vector2 pos = position + coord;
                if (tilesDistance.ContainsKey(pos))
                    continue;
                tilesDistance.Add(pos, distance);
            }
        }

        if (distance > MapSize + MapBorderRandomFactor)
            return;

        GenerateBorder(distance + 1);
    }

    private void FillOcean(int distance)
    {
        if (distance > MapSize + OceanSize + MapBorderRandomFactor)
            return;

        Vector2[] tilesPos = tilesDistance.Where(kvp => kvp.Value == distance - 1).Select(kvp => kvp.Key).ToArray();

        foreach (Vector2 position in tilesPos)
        {
            foreach (Vector2 coord in Tile.NeighbourCoords)
            {
                Vector2 pos = position + coord;
                if (tilesDistance.ContainsKey(pos))
                    continue;
                tilesDistance.Add(pos, distance);
                SpawnNewTile(TileOceanPrefab, pos);
            }
        }

        FillOcean(distance + 1);
    }

    //private void GenerateTiles(int distance)
    //{
    //    if (distance > MapSize)
    //        return;

    //    Tile[] tiles = tilesDistance.Where(kvp => kvp.Value == distance - 1).Select(kvp => kvp.Key).ToArray();

    //    foreach (Tile tile in tiles)
    //    {
    //        foreach (Vector2 coord in Tile.NeighbourCoords)
    //        {
    //            Vector2 pos = tile.Position + coord;
    //            if (Map.Tiles.ContainsKey(pos))
    //                continue;
    //            tilesDistance.Add(SpawnRandomTile(pos), distance);
    //        }
    //    }

    //    GenerateTiles(distance + 1);
    //}

    //private void GenerateOcean(int distance)
    //{
    //    if (distance > MapSize + OceanSize)
    //        return;

    //    Tile[] tiles = tilesDistance.Where(kvp => kvp.Value == distance - 1).Select(kvp => kvp.Key).ToArray();

    //    foreach (Tile tile in tiles)
    //    {
    //        foreach (Vector2 coord in Tile.NeighbourCoords)
    //        {
    //            Vector2 pos = tile.Position + coord;
    //            if (Map.Tiles.ContainsKey(pos))
    //                continue;
    //            tilesDistance.Add(SpawnNewTile(TileOceanPrefab, pos), distance);
    //        }
    //    }

    //    GenerateOcean(distance + 1);
    //}

    public Tile SpawnNewTile(Transform prefab, Vector2 position)
    {
        Tile newTile = Instantiate(prefab, (Vector2)transform.position + HexToSqrPosition(position), Quaternion.identity, transform).GetComponent<Tile>();
        newTile.Position = position;
        newTile.Map = Map;
        Map.Tiles.Add(newTile.Position, newTile);
        NetworkServer.Spawn(newTile.gameObject);
        return newTile;
    }

    private Tile SpawnRandomTile(Vector2 position)
    {
        Transform prefab = TilePlainsPrefab;

        float random = UnityEngine.Random.value;
        float chance = 0;

        foreach (var tileChance in tilesFrequencies)
        {
            chance += tileChance.Value;
            if (random <= chance)
            {
                prefab = tileChance.Key;
                break;
            }
        }

        return SpawnNewTile(prefab, position);
    }

    public void RemoveTile(Vector2 position)
    {
        if (!Map.Tiles.ContainsKey(position))
            return;

        Destroy(Map.Tiles[position].gameObject);
        Map.Tiles.Remove(position);

    }

    public Building SpawnNewBuilding(Transform prefab, Vector2 position)
    {
        return SpawnNewBuilding(prefab, Map.Tiles[position]);
    }

    public Building SpawnNewBuilding(Transform prefab, Tile tile)
    {
        Building newBuilding = Instantiate(prefab, HexToSqrPosition(tile.Position), Quaternion.identity, tile.transform).GetComponent<Building>();
        newBuilding.MoveBuilding(tile);
        NetworkServer.Spawn(newBuilding.gameObject);
        return newBuilding;
    }

    private static readonly float HexToSqrX = Mathf.Sqrt(3) / 2f;
    private static readonly float HexToSqrY = 0.5f;
    public Vector2 HexToSqrPosition(Vector2 position)
    {
        return new Vector2(position.x * HexToSqrX, position.x * HexToSqrY + position.y) * 0.85f * TileSpace;
    }

}
