﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class MapCustomEditor : MonoBehaviour
{
    public Map Map;
    public Transform CurrentTilePrefab;
    public Tile SelectedTile;

    public MapGenerator Generator => Map.Generator;

    private void Awake()
    {
        Map = GetComponent<Map>();
    }

    public void ChooseTilePrefab(Transform tilePrefab)
    {
        CurrentTilePrefab = tilePrefab;
    }

    public bool ReplaceTile(Vector2 position)
    {
        if (CurrentTilePrefab == null)
            return false;

        Vector2 pos = TransformPointToTilePosition(position);
        Map.Generator.RemoveTile(pos);
        Map.Generator.SpawnNewTile(CurrentTilePrefab, pos);
        return true;
    }

    public bool TryPlaceTile(Vector2 position)
    {
        if (CurrentTilePrefab == null)
            return false;

        Vector2 pos = TransformPointToTilePosition(position);
        if (Map.Tiles.ContainsKey(pos))
            return false;
        Map.Generator.SpawnNewTile(CurrentTilePrefab, pos);
        return true;
    }

    public Vector2 TransformPointToTilePosition(Vector2 point)
    {
        foreach (Vector2 pos in Map.Tiles.Keys)
        {
            if (IsPointInsideHexagon(point, Map.Generator.HexToSqrPosition(pos), 1))
                return pos;
        }

        return Map.Tiles.Keys.FirstOrDefault();
    }

    public void SelectTile(Tile tile)
    {
        SelectedTile = tile;


        //Generator.SpawnNewBuilding(Generator.VillagePrefab, SelectedTile);
        //Debug.Log("Spawning new village");
    }

    public bool IsPointInsideHexagon(Vector2 point, Vector2 hexagonCenter, float hexagonDiameter)
    {
        float dx = 2 * Mathf.Abs(point.x - hexagonCenter.x) / hexagonDiameter;
        float dy = 2 * Mathf.Abs(point.y - hexagonCenter.y) / hexagonDiameter;
        float s3 = Mathf.Sqrt(3);
        return dy <= s3 / 2 && s3 * dx + dy <= s3;
    }

}
