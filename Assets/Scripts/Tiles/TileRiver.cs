﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class TileRiver : Tile
{

    public override Resource Resource => Resource.None;

    public Vector2 DirectionStart;
    public Vector2 DirectionEnd;

    public Vector2 SetRandomEndDirection(Vector2 start)
    {
        DirectionStart = start;
        DirectionEnd = DirectionStart;

        Vector2[] forwardDirs = GetForwardDirections(start);

        while (DirectionEnd == DirectionStart)
            DirectionEnd = forwardDirs[UnityEngine.Random.Range(0, forwardDirs.Length)];
        return DirectionEnd;
    }

}
