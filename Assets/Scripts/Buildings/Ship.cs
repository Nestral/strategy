﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ship : Building
{

    public override bool Movable => true;

    public override Dictionary<Resource, int> BuildCostDictionary => BuildCost;

    private static readonly Dictionary<Resource, int> BuildCost = new Dictionary<Resource, int>()
    {
        { Resource.Wood, 3 }
    };

}
